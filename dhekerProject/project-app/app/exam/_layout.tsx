import React from 'react';
//import Promotion from './promotion';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { AntDesign, Ionicons } from '@expo/vector-icons';
import { Stack, router } from 'expo-router';

const _layout = () => {

  return (
    <Stack
      screenOptions={{
        title: "",
        headerLeft: () => (
          <TouchableOpacity style={{ marginLeft: 24 }} onPress={() => router.back()}>
            <AntDesign name="arrowleft" size={24} color="black" />
          </TouchableOpacity>
        ),
        headerRight: () => (
          <View style={styles.notificationContainer}>
            <View style={styles.notification}>
             
            </View>
            <TouchableOpacity
              style={{ marginRight: 14 }}
              onPress={() => {
                router.push({ pathname: "(aux)/notification" });
              }}
            >
              <Ionicons
                name="notifications-outline"
                size={24}
                color="black"
              />
            </TouchableOpacity>
          </View>
        ),
      }}
    ></Stack>
  )
}

export default _layout

const styles = StyleSheet.create({
  notificationContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  notification: {
    marginRight: 10
  },
  iconfilled: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  vectorStroke: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 2,
    bottom: 2,
    left: 3,
    overflow: "visible",
  },
  label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Roboto",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20,
  },
});
