
import { StyleSheet, Text, View } from 'react-native';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
//import { service } from '../../../service';
import {CardExam} from '../../Components/CardExam';


const Card = () => {
  const products = [{
    "title" : "test"
  },
  {
    "title" : "test2"
  }]


  return (
    <View style={styles.recommendedforyou}>
      <View style={styles.title}>
        <Text style={styles._recommendedforyou}>
          {`Recommandé pour vous`}
        </Text>
      </View>
      {
        products.map((item, index) => (
          <View style={styles.cardstyle3}>
            <CardExam key={index} title={item.title} />
          </View>
        ))
      }
    </View>
  );
};

export default Card;

const styles = StyleSheet.create({
  recommendedforyou: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 24,
    paddingBottom: 0,
    alignItems: "flex-start",
    rowGap: 12,
    paddingHorizontal: 24
  },
  title: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0
  },
  _recommendedforyou: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24
  },
  cardstyle3: {
    flexShrink: 0,
    width: 312,
    backgroundColor: "rgba(235, 238, 243, 1)",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 4,
    shadowColor: "rgb(0, 0, 0)",
    shadowOpacity: 0.25,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12
  },
});
