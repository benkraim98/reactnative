export default () => {
  return {
    expo: {
      extra: {
        storybookEnabled: process.env.storybookEnabled,
        eas: {
          projectId: '3853735f-ad6f-4865-89c6-1e20e559e544'
        }
      },
      scheme: 'your-app-iit',
      name: 'project-app',
      slug: 'project-app',
      version: '1.0.0',
      orientation: 'portrait',
      icon: './assets/icon.png',
      userInterfaceStyle: 'light',
      splash: {
        image: './assets/splash.png',
        resizeMode: 'contain',
        backgroundColor: '#ffffff'
      },
      assetBundlePatterns: [
        '**/*'
      ],
      ios: {
        supportsTablet: true
      },
      android: {
        adaptiveIcon: {
          foregroundImage: './assets/adaptive-icon.png',
          backgroundColor: '#ffffff'
        }
      },
      updates: {
        url: "https://u.expo.dev/3853735f-ad6f-4865-89c6-1e20e559e544"
      },
      web: {
        favicon: './assets/favicon.png',
        bundler: 'metro'
      },
      runtimeVersion: {
        policy: 'appVersion'
      },
      plugins: [
        'expo-router'
      ]
    }
  }
}
